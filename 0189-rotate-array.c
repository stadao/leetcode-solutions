//Given an integer array nums, rotate the array to the right by k steps, where k is non-negative.
//
//Example 1:
//
//Input: nums = [1,2,3,4,5,6,7], k = 3
//Output: [5,6,7,1,2,3,4]
//Explanation:
//rotate 1 steps to the right: [7,1,2,3,4,5,6]
//rotate 2 steps to the right: [6,7,1,2,3,4,5]
//rotate 3 steps to the right: [5,6,7,1,2,3,4]
//
//Example 2:
//
//Input: nums = [-1,-100,3,99], k = 2
//Output: [3,99,-1,-100]
//Explanation: 
//rotate 1 steps to the right: [99,-1,-100,3]
//rotate 2 steps to the right: [3,99,-1,-100]
//
//Constraints:
//
//    1 <= nums.length <= 10^5
//    -2^31 <= nums[i] <= 2^31 - 1
//    0 <= k <= 10^5
//
// 
//
//Follow up:
//
//    Try to come up with as many solutions as you can. There are at
//    least three different ways to solve this problem.  Could you do
//    it in-place with O(1) extra space?

#include <stdio.h>
#include <stdlib.h>

int abs(int a) {
    // returns |a|. Doesn't check for overflow, since it won't happen
    // given the constraints.
    return (a >= 0) ? a : (-a);
}

int gcd(int a, int b) {
    int m = abs(a);
    int n = abs(b);
    int r;

    if (m == 0 && n == 0) {
	return 0;
    }
    else if (m == 0) {
	return n;
    }
    else if (n == 0) {
	return m;
    }

    // m != 0 and n != 0
    do {
	r = m % n;
	m = n;
	n = r;
    } while (r != 0);

    return m;
}


inline int mod(a, m) {
    // returns a mod m in {0,1,2,...,m), assuming m>0
    if (a >= 0) return a % m;
    else return (m + (a % m)) % m;
}

void rotate(int* nums, int numsSize, int k){
    // the disjoint cycles (i, (i-k) mod numsSize, (i-2k) mod
    // numsSize, ...)  have lenght numsSize/gcd(k, numsSize). There are
    // gcd(k, numsSize) such cycles to be rotated.

    int temp;

    for (int i = 0; i < gcd(k, numsSize); i++) {
	temp = nums[i];
	for (int j = 0; j < numsSize/gcd(k, numsSize); j++) {
	    nums[mod(i-j*k, numsSize)] = nums[mod(i-(j+1)*k, numsSize)];
	}
	nums[(i+k) % numsSize] = temp;
    }
}


int main(void) {
    int n, k;
    int* a;

    scanf("%d", &n);
    a = (int*)calloc(n, sizeof(int));

    for (int i = 0; i < n; i++) {
	scanf("%d", a + i);
    }
    
    scanf("%d", &k);

    rotate(a, n, k);

    printf("\n");
    
    for (int i = 0; i < n;  i++) {
	printf("%d ", a[i]);
    }
    
    printf("\n");
}
