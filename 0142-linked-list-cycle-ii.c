// Given the head of a linked list, return the node where the cycle
// begins. If there is no cycle, return null.
// 
// There is a cycle in a linked list if there is some node in the list
// that can be reached again by continuously following the next
// pointer. Internally, pos is used to denote the index of the node
// that tail's next pointer is connected to (0-indexed). It is -1 if
// there is no cycle. Note that pos is not passed as a parameter.
// 
// Do not modify the linked list.
// 
// Input: head = [3,2,0,-4], pos = 1
// Output: tail connects to node index 1 Explanation: There is a cycle
// in the linked list, where tail connects to the second node.
// 
// Input: head = [1,2], pos = 0
// Output: tail connects to node index 0 Explanation: There is a cycle
// in the linked list, where tail connects to the first node.
// 
// Input: head = [1], pos = -1
// Output: no cycle
// Explanation: There is no cycle in the linked list.

//Constraints:
//
//The number of the nodes in the list is in the range [0, 10^4].
//-10^5 <= Node.val <= 10^5
//pos is -1 or a valid index in the linked-list.


/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

static const int stamp = 1234567; // bigger in absolute value than p->val
static const int max_abs_value = 100000;

void stamp_node(struct ListNode *p) {
     // after this function is applied to the node, we know it has been visited because its val
     // lies outside the range of possible values given in the constraints.
     
     // this modifies the linked list.
     
     if (p->val >=0) {
	  p->val += stamp;
     }
     else p->val -= stamp;
}

inline int is_stamped(struct ListNode *p) {
     // returns 1 if the node has been stamped, 0 otherwise
     return (p->val > max_abs_value || p->val < -max_abs_value) ? 1 : 0;
}

inline void unstamp_node(struct ListNode *p) {
     // the original traversal modifies every node. We undo it.
     // first node by node.
     // Assumes the node is_stamped

     if (p->val >= 0) {
	  p->val -= stamp;
     }
     else {
	  p->val += stamp;
     }
    
}

void unstamp(struct ListNode *head) {
     struct ListNode *p;

     p = head;

     while (p && is_stamped(p)) {
	  unstamp_node(p); // unstamp checks p->val
	  p = p->next;
     }
}



struct ListNode *detectCycle(struct ListNode *head) {
     // this is a simple O(n) algorithm that traverses the list
     // stamping its nodes until we find a stamped node or the list
     // ends.
  
     struct ListNode *p;

     p = head;

     if (!p) {
	  return p;
     }

     while (p->next) {
	  if (is_stamped(p)) {
	       unstamp(head);
	       return p;
	  }
	  else {
	       stamp_node(p);
	       p = p->next;
	  }
     }

     // if we are here, there is no cycle
     unstamp(head);
     return NULL;
}


int main(void) {
     // this time I did'n write a complete program, but the algorithm works
     return 0;
}
