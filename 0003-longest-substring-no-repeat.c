// Given a string s, find the length of the longest substring without
// repeating characters.

// Example: s = "abcabcbb"
// output: 3 ("abc")

// Example: s = "pwwkew"
// output: 3 ("wke")

// s consists of English letters, digits, symbols and spaces.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int oldlengthOfLongestSubstring(char * s){
     char hash[128];

     int max = 0; // stores the maximum length found so far
     int local_max = 0; // stores the maximum length found starting at
			// a given position
     int p_min, p_max; // positions of the first and last characters
		       // we are examining
     
     for (int i=0; i < 128; i++) {
	  hash[i] = 0;
     }

     p_min = 0;

     while (s[p_min] != 0) {
	  p_max = p_min;
	  local_max = 0;
	  while (s[p_max] != 0 && hash[s[p_max]] == 0) { // found a new character
	       hash[s[p_max]]++; // register that we found this character
	       p_max++;
	       local_max++;
	  }

	  if (local_max > max) max = local_max;

	  // we must clear the hash array, advance p_min and iterate.
	  if (hash[s[p_max]] > 0) {
	       for (int i=p_min; i <= p_max; i++) {
		    hash[s[i]] = 0;
	       }
	       p_min++;
	  }	       
     }

     return max;
}

int lengthOfLongestSubstring(char * s){
     char hash[128];

     int max = 0; // stores the maximum length found so far
     int local_max = 0; // stores the maximum length found starting at
			// a given position
     int p_min, p_max; // positions of the first and last characters
		       // we are examining
     
     memset(hash, 0, 128*sizeof(char));

     p_min = 0;
     p_max = 0;
     local_max = 0;

     while (s[p_min] != 0) {
	  while (s[p_max] != 0 && hash[s[p_max]] == 0) { // found a new character
	       hash[s[p_max]]++; // register that we found this character
	       p_max++;
	       local_max++;
	  }

	  if (local_max > max) max = local_max;

	  // why did we leave the loop?  if it is because we reached
	  // the end of the string, we may return
	  if (s[p_max] == 0) return max;

	  // if we left the loop because we found a repeated char, we
	  // advance one position with p_min and continue
	  if (hash[s[p_max]] > 0) {
	       hash[s[p_min]]--;
	       p_min++;
	       local_max--;
	  }	       
     }

     // this point should never be reached...
     return max;
}


int main(void) {
     char *a = "abcaabd";
     char *b = "pwwkew";
     char t[50001];

     //printf("%s, %d\n", a, lengthOfLongestSubstring(a));
     //printf("%s, %d\n", b, lengthOfLongestSubstring(b));

     while (1) {
	  scanf("%s", t);
	  printf ("%s, %d, %d\n", t, oldlengthOfLongestSubstring(t), lengthOfLongestSubstring(t));
     }
}
