#include <stdio.h>
#include <stdlib.h>

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode {
     int val;
     struct ListNode *next;
};


struct ListNode* build_list(int *x, int n) {
     // builds a list from the array x of n >= 1 elements

     struct ListNode *res, *aux;

     res = (struct ListNode*)malloc(sizeof(struct ListNode)); // no checks, live on the edge!
     aux = res;

     for (int i = 0; i < n; i++) {
	  aux->val = x[i];
	  if (i < n-1) {
	       aux->next = (struct ListNode*)malloc(sizeof(struct ListNode));
	       aux = aux->next;
	  }
	  else{
	       aux->next = NULL;
	  }
     }

     return res;
}


void print_list(struct ListNode *l) {
     struct ListNode *aux;

     aux = l;
     while (aux !=NULL) {
	  printf("%d ", aux->val);
	  aux = aux->next;
     }
     printf("\n");
}


struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2){
    struct ListNode *result, *walker, *p1, *p2;
    int nodes_sum;
    int carry = 0;

    // first node
    nodes_sum = l1->val + l2->val;
    carry = (nodes_sum > 9) ? 1 : 0;
    nodes_sum = (nodes_sum > 9) ? (nodes_sum-10) : (nodes_sum);

    result = walker = (struct ListNode*)malloc(sizeof(struct ListNode));
    walker->val = nodes_sum;

    p1 = l1->next;
    p2 = l2->next;
    while (p1 != NULL || p2 != NULL) {
        walker->next = (struct ListNode*)malloc(sizeof(struct ListNode));
        walker = walker->next;
        nodes_sum = (p1 != NULL) ? p1->val : 0;
        nodes_sum = (p2 != NULL) ? nodes_sum + p2->val : nodes_sum;
        nodes_sum += carry;
        carry = (nodes_sum > 9) ? 1 : 0;
        nodes_sum = (nodes_sum > 9) ? (nodes_sum-10) : (nodes_sum);
        walker->val = nodes_sum;
        
        p1 = (p1 != NULL) ? p1->next: p1;
        p2 = (p2 != NULL) ? p2->next: p2;
    }

    if (carry == 1) {
        walker->next = (struct ListNode*)malloc(sizeof(struct ListNode));
        walker = walker->next;
        walker->val = 1; //carry
    }

    walker->next = NULL;

    return result;
}


int main(void) {
     struct ListNode *l1, *l2, *r, *aux;

     int x[] = {1, 9, 9, 2, 8};
     int y[] = {3, 2, 0, 1, 2};

     l1 = build_list(x, 2);
     l2 = build_list(y, 5);

     print_list(l1);
     print_list(l2);

     r = addTwoNumbers(l1, l2);

     print_list(r);
}
