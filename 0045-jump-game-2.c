// You are given a 0-indexed array of integers nums of length n. You
// are initially positioned at nums[0].
// 
// Each element nums[i] represents the maximum length of a forward
// jump from index i. In other words, if you are at nums[i], you can
// jump to any nums[i + j] where:
// 
//     0 <= j <= nums[i] and
//     i + j < n
// 
// Return the minimum number of jumps to reach nums[n - 1]. The test
// cases are generated such that you can reach nums[n - 1].
// 
//  
// 
// Example 1:
// 
// Input: nums = [2,3,1,1,4]
// Output: 2
// Explanation: The minimum number of jumps to reach the last index is
// 2. Jump 1 step from index 0 to 1, then 3 steps to the last index.
// 
// Example 2:
// 
// Input: nums = [2,3,0,1,4]
// Output: 2
// 
//  
// 
// Constraints:
// 
//     1 <= nums.length <= 104 0 <= nums[i] <= 1000 It's guaranteed
//     that you can reach nums[n - 1].

int jump(int* nums, int numsSize){
    // algorithm:
    // we will determine the positions that can be reached in k steps for k >= 0
    // -> k=0: p[0] = {0}
    // -> for each k, p[k] is made up of consecutive numbers
    // -> if p[k]   = [a0, a0+1, ..., a0+m] (this represents an "integer interval"),
    //       p[k+1] = [a0+m+1, ..., max{a0+nums[a0], a0+1 + nums[a0+1],...}]

    int minimum, maximum, k;
    int new_minimum, new_maximum;

    k = 0;
    minimum = 0; // minimum position that can be reached with exactly k jumps
    maximum = 0; // maximum position that can be reached with exactly k jumps

    while (maximum < numsSize - 1) {
	k++;
	new_minimum = maximum + 1;
	new_maximum = new_minimum;
	
	for (int j = minimum; j <= maximum; j++ ) {
	    if (j + nums[j] > new_maximum) {
		new_maximum = j + nums[j];
	    }
	}

	minimum = new_minimum;
	maximum = new_maximum;
    }

    return k;
}
