// Given a string s, return the longest
// palindromic
// substring
// in s.
// 
// Example 1:
// Input: s = "babad"
// Output: "bab"
// Explanation: "aba" is also a valid answer.
// 
// Example 2:
// Input: s = "cbbd"
// Output: "bb"
// 
// Constraints:
//     1 <= s.length <= 1000
//     s consist of only digits and English letters.

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int isPalindrome(char *s, unsigned long start, unsigned long end) {
    // returns true if the substring s[start]....s[end] (inclusive) is a
    // palindrome.

    char *temp;

    temp = (char *)malloc(end-start+2);
    temp[end-start+1] = 0;
    strncpy(temp, s+start, end-start+1);
    
    printf("(isP) string: %s, start: %lu, end: %lu\n", s, start, end);
    
    
    for (int i = 0; i <= (end-start)/2; i++) {
	if (*(s+start+i) != *(s+end-i)) {
	    printf("not palindrome: %s\n", temp);
	    free(temp);
	    return 0; // not palindrome		    
	}
    }
    printf("palindrome: %s\n", temp);
    free(temp);
    return 1;
}

char *palindromeKnownLength(char *s, unsigned long length, unsigned long n) {
    // searchs for a palindrome with exactly n characters in the string s with
    // length characters. Returns a pointer to the first character of a found
    // palindrome, and NULL otherwise.

    printf("(pKL) string: %s, length: %lu, size: %lu\n", s, length, n);
    
    for (unsigned long i = 0; i < length-n+1; i++) {
	if (isPalindrome(s, i, i+n-1)) {	    
	    return (s+i);
	}
    }
    return NULL;
}

char *longestPalindrome(char * s){
    //returns the longest palindromic substring of s,
    // which has length >= 1.
    
    unsigned long len = strlen(s);
    unsigned long max_p = 1; // the longest length for which a palindrome was found in s
    unsigned long min_np = len; // the smallest length for which we know there is no palindrome in s
                                // of that length or longer
    unsigned long mid;
    char *pali = s;
    char *temp = NULL;
    char *answer;

    if (palindromeKnownLength(s, len, len)) return s;

    while (min_np - max_p > 1) {
	mid = (min_np + max_p)/2;
	if ((temp = palindromeKnownLength(s, len, mid))) {
	    max_p = mid;
	    pali = temp; // we save the position of the found palindrome
	}
	else {
	    //if there is no palindrome of size mid or mid+1, then
	    //min_np = mid
	    if ((temp = palindromeKnownLength(s, len, mid+1))) {
		max_p = mid + 1;
		pali = temp;
	    }
	    else {
		min_np = mid;
	    }
	}
    }

    answer = (char *)malloc(max_p + 1);
    answer[max_p] = 0;
    strncpy(answer, pali, max_p); // we use the saved position to avoid computing it again
    return answer;
}


int main(void) {
    char test[1001];
    int start, end, n;

    while (1) {
	scanf("%s", test);
	printf("Longest palindromic substring: %s\n\n", longestPalindrome(test));
    }
}
