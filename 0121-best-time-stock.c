//You are given an array prices where prices[i] is the price of a
//given stock on the ith day.
//
//You want to maximize your profit by choosing a single day to buy one
//stock and choosing a different day in the future to sell that stock.
//
//Return the maximum profit you can achieve from this transaction. If
//you cannot achieve any profit, return 0.
//
//Example 1:
//
//Input: prices = [7,1,5,3,6,4]
//Output: 5
//Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6),
//profit = 6-1 = 5.  Note that buying on day 2 and selling on day 1 is
//not allowed because you must buy before you sell.
//
//Example 2:
//
//Input: prices = [7,6,4,3,1]
//Output: 0
//Explanation: In this case, no transactions are done and the max
//profit = 0.
//
//Constraints:
//
//    1 <= prices.length <= 10^5
//    0 <= prices[i] <= 10^4

#include <stdio.h>
#include <stdlib.h>

int maxProfit(int* prices, int pricesSize){
    int max_profit = 0;
    int buy_candidate = 0; // index of prices array
    int i = 0;

    while (i < pricesSize) {
	if (prices[i] - prices[buy_candidate] > max_profit) {
	    max_profit = prices[i] - prices[buy_candidate];
	}
	else if (prices[i] < prices[buy_candidate]) {
	    // in this case, we have already found the maximum profit when we buy on
	    // the buy_candidate day, and we start testing a new buying day
	    buy_candidate = i;
	}
	i++;
    }

    return max_profit;
}


int main(void) {
    int size;
    int *p;

    scanf("%d", &size);

    p = (int*)calloc(size, sizeof(int));

    for (int i = 0; i < size; i++) {
	scanf("%d", p+i);
    }

    printf("max profit: %d\n", maxProfit(p, size));
}
