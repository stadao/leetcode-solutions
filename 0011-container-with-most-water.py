#!/usr/bin/python3

# You are given an integer array height of length n. There are n
# vertical lines drawn such that the two endpoints of the ith line are
# (i, 0) and (i, height[i]).
# 
# Find two lines that together with the x-axis form a container, such
# that the container contains the most water.
#
# Return the maximum amount of water a container can store.

# the solution below is O(n^2) and too slow

class Solution(object):
    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        n = len(height)

        max_area = 0
        for i in range(n-1):
            for j in range(n-1-i):
                k = j+i+1
                area = (k-i)*min(height[i], height[k])
                if area > max_area:
                    max_area = area
        
        return max_area
