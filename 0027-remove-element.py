#!/usr/bin/python3

# Given an integer array nums and an integer val, remove all
# occurrences of val in nums in-place. The order of the elements may
# be changed. Then return the number of elements in nums which are not
# equal to val.

# Consider the number of elements in nums which are not equal to val
# be k, to get accepted, you need to do the following things:

# Change the array nums such that the first k elements of nums contain
# the elements which are not equal to val. The remaining elements of
# nums are not important as well as the size of nums.

# Return k.

# the solution given below uses the following simple idea: every time
# we find val, we drag it to the end of the list and bring the element
# that was in the end to take its place. We don't use any Python
# specific function.

class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        k = len(nums)    # we have found 0 ocurrences of val so far...
        pos = 0          # the index of nums to be inspected next
        last_pos = k-1   # the highest index that needs to be inspected

        # we will drag every val to the end of nums    
        while pos <= last_pos:
            while nums[pos] == val and pos <= last_pos:
                k -= 1
                nums[pos], nums[last_pos] = nums[last_pos], nums[pos]
                last_pos -=1
            pos += 1

        return k
