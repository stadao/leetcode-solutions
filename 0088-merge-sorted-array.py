#!/usr/bin/python3

#You are given two integer arrays nums1 and nums2, sorted in
#non-decreasing order, and two integers m and n, representing the
#number of elements in nums1 and nums2 respectively.
#
#Merge nums1 and nums2 into a single array sorted in non-decreasing order.
#
#The final sorted array should not be returned by the function, but
#instead be stored inside the array nums1. To accommodate this, nums1
#has a length of m + n, where the first m elements denote the elements
#that should be merged, and the last n elements are set to 0 and
#should be ignored. nums2 has a length of n.
#
# 
#
#Example 1:
#
#Input: nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
#Output: [1,2,2,3,5,6]
#Explanation: The arrays we are merging are [1,2,3] and [2,5,6].
#The result of the merge is [1,2,2,3,5,6] with the underlined elements
#coming from nums1.
#
#Example 2:
#
#Input: nums1 = [1], m = 1, nums2 = [], n = 0
#Output: [1]
#Explanation: The arrays we are merging are [1] and [].
#The result of the merge is [1].
#
#Example 3:
#
#Input: nums1 = [0], m = 0, nums2 = [1], n = 1
#Output: [1]
#Explanation: The arrays we are merging are [] and [1].
#The result of the merge is [1].
#Note that because m = 0, there are no elements in nums1. The 0 is
#only there to ensure the merge result can fit in nums1.
#
#Constraints:
#
#    nums1.length == m + n
#    nums2.length == n
#    0 <= m, n <= 200
#    1 <= m + n <= 200
#    -109 <= nums1[i], nums2[j] <= 109
#
#Follow up: Can you come up with an algorithm that runs in O(m + n) time?

from typing import List

class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:

        # nums3 stores elements of nums1 that had to give way to merged elements coming from nums2
        nums3 = []

        p2 = 0 # index of the first element of nums2 not yet merged
        p3 = 0 # index of the first element of nums3 not yet merged

        # m_candidates will have the keys 1, 2 and 3, at most, and
        # each key will hold the value of the first non merged element
        # of nums1, nums2 and nums3, respectively. The point for its
        # existence is that one or more lists may have no more
        # elements to be merged.
        m_candidates = {}

        # p1 will run through nums1, and at each iteration nums1[p1]
        # will we be set to a new merged element.

        for p1 in range(m):
            # when p1 < m, the new merged element may come from nums1,
            # nums2 or nums3

            m_candidates = {}
            
            m_candidates[1] = nums1[p1]
            if p2 < n:
                m_candidates[2] = nums2[p2]
            if len(nums3) > 0 and p3 < len(nums3):
                m_candidates[3] = nums3[p3]

            val_to_merge = min(m_candidates.values())

            if 1 in m_candidates.keys() and nums1[p1] == val_to_merge:
                # nothing to do
                pass
            elif 2 in m_candidates.keys() and nums2[p2] == val_to_merge:
                # nums1[p1] needs to give space for nums2[p2]
                nums3.append(nums1[p1])
                nums1[p1] = nums2[p2] # == val_to_merge
                p2 += 1
            elif 3 in m_candidates.keys() and nums3[p3] == val_to_merge:
                # nums1[p1] needs to give space for nums3[p3]
                nums3.append(nums1[p1])
                nums1[p1] = nums3[p3] # == val_to_merge
                p3 += 1
            else:
                print('This should never happen')
                pass

        for p1 in range(m,m+n):
            # now we are only comparing nums2 and nums3

            m_candidates = {}
            
            if p2 < n:
                m_candidates[2] = nums2[p2]
            if len(nums3) > 0 and p3 < len(nums3):
                m_candidates[3] = nums3[p3]

            val_to_merge = min(m_candidates.values())

            if 2 in m_candidates.keys() and nums2[p2] == val_to_merge:
                nums1[p1] = nums2[p2] # == val_to_merge
                p2 += 1
            elif 3 in m_candidates.keys() and nums3[p3] == val_to_merge:
                nums1[p1] = nums3[p3] # == val_to_merge
                p3 += 1
            

if __name__ == '__main__':
    while True:
        n1 = [int(x) for x in input('nums1: ').split()]
        m = len(n1)
        n2 = [int(x) for x in input('nums2: ').split()]
        n = len(n2)

        n1 = n1 + [0 for _ in range(n)]

        s = Solution()
        
        s.merge(n1, m, n2, n)
        print(n1)
