# Leetcode solutions

This is where I keep my solutions to LeetCode problems. Each filename begins with the number problem from LeetCode. Algorithms are implemented mostly in C or Python, and queries are written choosing the MySQL syntax, although these things can change at any time.