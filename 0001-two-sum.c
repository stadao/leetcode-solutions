//Given an array of integers nums and an integer target, return
//indices of the two numbers such that they add up to target.

//You may assume that each input would have exactly one solution, and
//you may not use the same element twice.

//You can return the answer in any order.

#include <stdlib.h>
#include <stdio.h>

// test qsort
int compare(const void* a, const void* b) {
     return (*(int*)a - *(int*)b);
}


int binarySearch(int* nums, int imin, int imax, int value) {
     // returns (any) j where nums[j] == value and imin <= j <= imax, or
     // -1 if not found. Assumes nums is ordered (increasing)
     
     int imid;
	
     if (imin == imax) {
	  return nums[imin] == value ? imin : -1;
     }
     else {
	  imid = (imin + imax)/2;
	  
	  if (nums[imid] == value) {
	       return imid;
	  }
	  else if (nums[imid] < value) {
	       return binarySearch(nums, imid+1, imax, value);
	  }
	  else if (nums[imid] > value) {
	       // imid may already be == imin, so we don't use imid-1
	       return binarySearch(nums, imin, imid, value);
	  }
     }
	
     return -1;
}


/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* twoSum(int* nums, int numsSize, int target, int* returnSize){
     // numsSize: number of elements in the array
     int *cnums; //copy of nums
     int *answer, a0, a1;
     int i, j, k, l;

     answer = (int*)malloc(2*sizeof(int));

     cnums = (int*)calloc(numsSize, sizeof(int));
     for (i = 0; i < numsSize; i++) {
	  cnums[i] = nums[i]; // we could use memcpy...
     }

     qsort(cnums, numsSize, sizeof(int), &compare);

     for (i = 0; i < numsSize-1; i++) {
	  j = binarySearch(cnums, i+1, numsSize-1, target-cnums[i]);
	  if (j >= 0) {
	       printf("%d %d\n", i, j);
	       // found the two positions i and j in the sorted array, with i<j.
	       // time to recover their positions original array nums:
	       for (k = 0; k < numsSize; k++) {
		    if (nums[k] == cnums[i]) {
			 a0 = k;
			 break;
		    }	     
	       }
	       for (k = numsSize-1; k >= 0; k--) {
		    if (nums[k] == cnums[j]) {
			 a1 = k;
			 break;
		    }
	       }

	       *returnSize = 2;
	       answer[0] = a0;
	       answer[1] = a1;
	       return answer;
	  }
     }
     *returnSize = 0;
     return NULL;
}



int main(void) {

     // copy the nums array
     // sort it and search
     // use the original array to recover the indices in the original array
     //qsort(x, n, sizeof(int), &compare);

     int len, target;
     int *x;
     int *ans;
     int retSize;
     
     while (1) {
	  printf("Enter the array length: ");
	  scanf("%d", &len);
	  printf("length = %d\n", len);
	  printf("Enter the array: ");

	  x = (int*)calloc(len, sizeof(int));
	  
	  for (int i=0; i < len; i++) {
	       scanf("%d", &x[i]);
	  }

	  printf("Enter target: ");
	  scanf("%d", &target);

	  ans = twoSum(x, len, target, &retSize);

	  printf("positions = [%d, %d]\n", ans[0], ans[1]);

	  free(x);
	  free(ans);
     }
}
