//Given an array of integers citations where citations[i] is the
//number of citations a researcher received for their ith paper,
//return the researcher's h-index.
//
//According to the definition of h-index on Wikipedia: The h-index is
//defined as the maximum value of h such that the given researcher has
//published at least h papers that have each been cited at least h
//times.
//
//Example 1:
//
//Input: citations = [3,0,6,1,5]
//Output: 3
//Explanation: [3,0,6,1,5] means the researcher has 5 papers in total
//and each of them had received 3, 0, 6, 1, 5 citations respectively.
//Since the researcher has 3 papers with at least 3 citations each and
//the remaining two with no more than 3 citations each, their h-index
//is 3.
//
//Example 2:
//
//Input: citations = [1,3,1]
//Output: 1
//
//Constraints:
//    n == citations.length
//    1 <= n <= 5000
//    0 <= citations[i] <= 1000

#include <stdlib.h>

int hIndex(int* citations, int citationsSize){
    int *acc;

    acc = (int*)calloc(citationsSize+1, sizeof(int));

    if (acc == NULL) {
	return -1; // error allocating memory
    }

    // first we count the number of papers that have i citations...
    for (int i = 0; i < citationsSize; i++) {
	// -> acc[i] will be equal to the number of papers that have
	// received exactly i citations, if i < citationsSize.
	// -> acc[citationsSize] will be equal to the number of papers
	// that have received at least citationsSize citations, since
	// the index number cannot be greater than citationsSize
	int index = citations[i] >= citationsSize ? citationsSize : citations[i];
	acc[index] += 1;
    }

    // now we go from the top of number of citations to the bottom, until the
    // accumulated number of papers is at least as big as the h_index candidate:
    int j = citationsSize;
    int acc_papers = acc[citationsSize];
    while (acc_papers < j) {
	acc_papers += acc[j-1];
	j--;
    }
    
    return j;
}
