//Given an array nums of size n, return the majority element.
//
//The majority element is the element that appears more than ⌊n / 2⌋
//times. You may assume that the majority element always exists in the
//array.
//
//Example 1:
//
//Input: nums = [3,2,3]
//Output: 3
//
//Example 2:
//
//Input: nums = [2,2,1,1,1,2,2]
//Output: 2
//
//Constraints:
//
//    n == nums.length
//    1 <= n <= 5 * 10^4
//    -10^9 <= nums[i] <= 10^9

// if we pick an element of the array, it will be the majority element
// with probability greater than 50%. So, if we pick random elements,
// the expected time to find the majority element is proportional to
// the sum
// n*(1/2 + 1/4 + 1/8 + ...) = 2n = O(n)

// we will implement a random number generator to avoid having a
// pattern of elements that makes our algorithm run in O(n^2).

#include <stdio.h>
#include <stdlib.h>


static long seed;

// we'll use a simple linear congruential generator
double my_random() {
    // returns a number x in the interval [0,1).

    static short initialized = 0;
    static long m = 2147483648; // 2^31
    static long a = 1103515245;
    static long c = 12345;
    static long x;

    if (initialized == 0) {
	initialized = 1;
	x = seed;
    }
    else {
	x = (a*x + c) % m;
    }
    return (double)x/(double)m;
}

int my_random_int(int upper) {
    // generates a random integer between 0 an upper-1

    return (int)(my_random()*upper);
}

int majorityElement(int* nums, int numsSize) {
    // we generate a random index i and test wheter or not nums[i] is the majority element.
    // the chances of not finding it are incredibly small.

    seed = numsSize;
    int i = 0;
    int counter = 0; // number of elements in the array equal to nums[i]
    
    do {
	i = my_random_int(numsSize);
	counter = 0;
	
	for (int j = 0; j < numsSize; j++) {
	    if (nums[j] == nums[i]) {
		counter++;
	    }
	}
    } while (counter <= numsSize/2);
    
    return nums[i];
}

int main(void) {
    int size;
    int *a;
    
    scanf("%d", &size);

    a = (int*)calloc(size, sizeof(int));

    for (int j = 0; j < size; j++) {
	scanf("%d", a+j);
    }

    for (int j = 0; j < size; j++) {
	printf("%d ", a[j]);
    }
    printf("\n");

    printf("majority: %d\n", majorityElement(a, size));

    return 0;
}
