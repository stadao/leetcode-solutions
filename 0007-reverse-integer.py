#!/usr/bin/python3

# Given a signed 32-bit integer x, return x with its digits
# reversed. If reversing x causes the value to go outside the signed
# 32-bit integer range [-231, 231 - 1], then return 0. Assume the
# environment does not allow you to store 64-bit integers (signed or
# unsigned).
#
#Constraints:
#    -2**31 <= x <= 2**31 - 1

class Solution:
    def maxArea(self, height: List[int]) -> int:

        pmin = 0
        pmax = len(height)-1

        max_area = pmax * min(height[0], height[pmax])

        while pmin < pmax:
            if height[pmin] < height[pmax]:
                pmin += 1
                area = (pmax-pmin)*min(height[pmin], height[pmax])
                if area > max_area:
                    max_area = area
            elif height[pmin] > height[pmax]:
                pmax -= 1
                area = (pmax-pmin)*min(height[pmin], height[pmax])
                if area > max_area:
                    max_area = area
            else:
                if height[pmin+1] > height[pmax-1]:
                    pmin +=1
                    area = (pmax-pmin)*min(height[pmin], height[pmax])
                    if area > max_area:
                        max_area = area
                else:
                    pmax -=1
                    area = (pmax-pmin)*min(height[pmin], height[pmax])
                    if area > max_area:
                        max_area = area
        
        return max_area

