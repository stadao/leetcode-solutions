#!/usr/bin/python3

# You are given a 0-indexed 2D integer array questions where
# questions[i] = [pointsi, brainpoweri].
#
# The array describes the questions of an exam, where you have to
# process the questions in order (i.e., starting from question 0) and
# make a decision whether to solve or skip each question. Solving
# question i will earn you pointsi points but you will be unable to
# solve each of the next brainpoweri questions. If you skip question
# i, you get to make the decision on the next question.
#
# For example, given questions = [[3, 2], [4, 3], [4, 4], [2, 5]]: If
# question 0 is solved, you will earn 3 points but you will be unable
# to solve questions 1 and 2. If instead, question 0 is skipped and
# question 1 is solved, you will earn 4 points but you will be unable
# to solve questions 2 and 3.
#
# Return the maximum points you can earn for the exam.


class Solution:
    def mostPoints(self, questions: List[List[int]]) -> int:

        ll = len(questions)
        # max_points_list[j] will store the maximum number of points we can score
        # if we can only pick questions starting from question j.
        # we need to compute max_points_list[0]
        max_points_list = [0 for i in range(ll)] # we still know nothing

        # we fill max_points_list bacwards,
        # p = ll-1, ll-2, ..., 0
        for p in range(ll-1,-1,-1):
            # n is the next available question if we answer question[p]
            n = p + questions[p][1] + 1
            if n >= ll: # end reached! No more questions for you if you answer question[p]
                if p == ll-1:
                    max_points_list[p] = questions[p][0]
                else:
                    max_points_list[p] = max(questions[p][0], max_points_list[p+1])
            else:
                max_points_list[p] = max(questions[p][0] + max_points_list[n],
                                         max_points_list[p+1])
        
        return max_points_list[0]
