// You are given an integer array nums. You are initially positioned
// at the array's first index, and each element in the array
// represents your maximum jump length at that position.
// 
// Return true if you can reach the last index, or false otherwise.
//
// Example 1:
// 
// Input: nums = [2,3,1,1,4]
// Output: true
// Explanation: Jump 1 step from index 0 to 1, then 3 steps to the
// last index.
// 
// Example 2:
// 
// Input: nums = [3,2,1,0,4]
// Output: false
// Explanation: You will always arrive at index 3 no matter what. Its
// maximum jump length is 0, which makes it impossible to reach the
// last index.
// 
// Constraints:
// 
//     1 <= nums.length <= 10^4
//     0 <= nums[i] <= 10^5

bool canJump(int* nums, int numsSize){
    int max_reachable = nums[0];
	
    int i = 0;
    // i runs through an imaginary array x[0], ..., x[max_reachable-1]
    while (i <= max_reachable && i < numsSize) {
	// this could be optimized by returning as soon as
	// max_reachable is at least numsSize-1
	if (i + nums[i] > max_reachable) {
	    max_reachable = i + nums[i];
	}
	i++;
    }
    return max_reachable >= numsSize - 1 ? true : false;
}

