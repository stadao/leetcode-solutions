// Given an integer n, you must transform it into 0 using the following operations any number of times:

// - Change the rightmost (0th) bit in the binary representation of n.
// - Change the ith bit in the binary representation of n if the (i-1)th bit is set to 1 and the (i-2)th through 0th bits are set to 0.

// Return the minimum number of operations to transform n into 0.

// Example 1:
// Input: n = 3
// Output: 2
// Explanation: The binary representation of 3 is "11".
// "11" -> "01" with the 2nd operation since the 0th bit is 1.
// "01" -> "00" with the 1st operation.

// Example 2:
// Input: n = 6
// Output: 4
// Explanation: The binary representation of 6 is "110".
// "110" -> "010" with the 2nd operation since the 1st bit is 1 and 0th through 0th bits are 0.
// "010" -> "011" with the 1st operation.
// "011" -> "001" with the 2nd operation since the 0th bit is 1.
// "001" -> "000" with the 1st operation.

// Constraints:

//     0 <= n <= 10^9

#include <iostream>


class Solution {
public:

    // this is much slower O(n), but gives the right result.
    // we simply go from 0 to n.
    int alternative(int n) {
	int i = 0;
	int next = 0;
	int k = 0;
	
	// we start at 0 and reach n
	while (i != n) {
	    i = op_next(i, next);
	    k++;
	}
	return k;
    }

    // if we start at 0 and keep applying the 2 operations, we get a
    // pattern where the first 2^n numbers are such that:
    //
    // - the first half has leading digit 0 (if we write every number
    // with n digits in base 2)
    //
    // - the second half has leading digit 1, and the remaining 4
    // digits appear in the reverse order that they appeared in the
    // first half. This leads to the recursive formula below.
    int minimumOneBitOperations(int n) {
	if (n == 0 || n == 1) return n;

	int k = digits2(n);

	// the first number is 2^(k-1)
	// if f = minimumOneBitOperations, the recursive formula for
	// f is
	// f(n) = 2^(k-1) + f(flip(flip(n,k), k-1))
	return (1 << (k-1)) + minimumOneBitOperations(flip(flip(n, k-1), k-2));
    }

    
private:

    int digits2(int n) {
	// returns the number of digits of n when written in base 2
	// n = 4 = 100_2 has 3 digits
	int d = 0;
	int q = n;

	while (q > 0) {
	    q = q >> 1;
	    d++;
	}
	return d;
    }

    int flip(int n, int i) {
	// flips the i th-digit of n
	// the rightmost digits is the "0 th".

	return n ^ (1 << i);
    }

    // n^k, handmade. Assumes k >= 0 and that overflows never happen!
    int power(int n, int k) {
	int q = k;
	int acc = 1;
	long n_power = n; // although acc doesn't get too big to
			  // overflow, we square n_power one extra
			  // time...

	while (q > 0) {
	    if (q % 2 == 1) {
		acc *= n_power;
	    }
	    q /= 2;
	    n_power *= n_power;
	}

	return acc;
    }


    // the first allowed operation
    int op_0(int n) {
	return flip(n, 0);
    }

    // the second operation
    int op_1(int n) {
	int q = n;
	int i = 0;

	while (q % 2 == 0) {
	    q /= 2;
	    i++;
	}

	return flip(n, i+1);
    }

    int op_next(int n, int& next) {
	if (next == 0) {
	    next = 1;
	    return op_0(n);
	}
	else if (next == 1) {
	    next = 0;
	    return op_1(n);
	}
	else // what?
	    return -1;
    }
	
};


int main() {
    Solution x;

    for (int n = 0; n < 20; n++) {
	std::cout << "n = " << n << ": " << std::endl
		  << x.minimumOneBitOperations(n) << " "
		  << x.alternative(n)
		  << std::endl;
    }

    std::cout << x.minimumOneBitOperations(78477515) << std::endl; // 121000077
    std::cout << x.minimumOneBitOperations(144243566) << std::endl; // 252751284

    return 0;
}
